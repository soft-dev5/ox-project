
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OS
 */
public class Main {

    public static void main(String[] args) {
        char[][] board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
                
            }
        }

        Scanner scan = new Scanner(System.in);
        System.out.println("Welcome to OX Game");

        boolean player1 = true;
        boolean gameEnded = false;

        while (!gameEnded) {
            drawBoard(board);

            if (player1) {
                System.out.println("'Turn O ");
            } else {
                System.out.println("'Turn X ");
            }

            char c = '-';
            if (player1) {
                c = 'O';
            } else {
                c = 'X';
            }

            int row = 0;
            int col = 0;

            while (true) {
                System.out.print("Please input row, col: ");
                row = scan.nextInt();
                System.out.print("Please input row, col: ");
                col = scan.nextInt();
                if (row < 0 || col < 0 || row > 3 || col > 3) {
                    System.out.println("This position is off the "
                            + "bounds of the board! Try again.");
                } else if (board[row-1][col-1] != '-') {
                    System.out.println("Someone has already made a "
                            + "move at this position! Try again.");
                } else {
                    break;
                }

            }

            board[row-1][col-1] = c;
            if (playerHasWon(board) == 'X') {
                System.out.println(" has won!");
                gameEnded = true;
            } else if (playerHasWon(board) == 'O') {
                System.out.println(" has won!");
                gameEnded = true;
            } else {
                if (boardIsFull(board)) {
                    System.out.println("It's a tie!");
                    gameEnded = true;
                } else {
                    player1 = !player1;
                }
            }
        }
        drawBoard(board);
        scan.close();
    }

    public static void drawBoard(char[][] board) {
        System.out.println("Board:");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    public static char playerHasWon(char[][] board) {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1]
                    && board[i][1] == board[i][2]
                    && board[i][0] != '-') {
                return board[i][0];
            }
        }

        for (int j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j]
                    && board[1][j] == board[2][j]
                    && board[0][j] != '-') {
                return board[0][j];
            }
        }

        if (board[0][0] == board[1][1]
                && board[1][1] == board[2][2]
                && board[0][0] != '-') {
            return board[0][0];
        }

        if (board[2][0] == board[1][1]
                && board[1][1] == board[0][2]
                && board[2][0] != '-') {
            return board[2][0];
        }

        return ' ';
    }

    public static boolean boardIsFull(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
}
